<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Auth\LoginAuthRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginAuthController extends Controller
{
    public function login(LoginAuthRequest $request)
    {
        $user = User::where(['email' => $request->email])->first();
        $credentials = request(['email', 'password']);
        if (is_null($user) || !$token = auth()->attempt($credentials)) return $this->failure('email or password is wrong!');
        if (!$user->email_verified) return $this->failure('your email not verified');
        return $this->success([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'eee' => auth()->user()->roles()->where('type', 'master')->exists()
        ]);
    }
}
