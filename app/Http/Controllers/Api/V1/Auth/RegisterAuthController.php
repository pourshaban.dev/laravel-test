<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\Auth\ConfirmCodeAuthRequest;
use App\Http\Requests\V1\Auth\RegisterAuthRequest;
use App\Jobs\SendRegisterEmailJob;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterAuthController extends Controller
{
    public function register(RegisterAuthRequest $request)
    {
        $user = User::query()->create(['name' => $request->name, 'email' => $request->email, 'password' => bcrypt($request->password)]);
        $token = rand(100000, 999999);
        $user->update(['email_token' => $token]);
        dispatch(new SendRegisterEmailJob($request->email, $token));
        return $this->success([], 'please check your email');
    }

    public function confirmCode(ConfirmCodeAuthRequest $request)
    {
        $user = User::query()->where(['email' => $request->email, 'email_token' => $request->token])->first();
        if (is_null($user)) return $this->failure('your token is invalid!');
        if ($user->email_verified) return $this->failure('your email is verified before!');
        $user->update(['email_verified' => true,]);
        return $this->success([], 'your email is verified , please login');
    }
}
