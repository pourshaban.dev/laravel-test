<?php

namespace App\Http\Controllers\Api\V1\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class IndexUsersController extends Controller
{
    public function index()
    {
        $users = User::query()->get();
        return $this->success($users);
    }
}
