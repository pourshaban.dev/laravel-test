<?php

namespace App\Http\Controllers;

use App\Traits\Api\V1\Response\ApiResponseTrait;
use App\Traits\Api\V1\Utilities\Utilities;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use Utilities, ApiResponseTrait, AuthorizesRequests, ValidatesRequests;
}
