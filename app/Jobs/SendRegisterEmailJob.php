<?php

namespace App\Jobs;

use App\Mail\RegisterMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendRegisterEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 2;

    public $maxExceptions = 2;

    private $email;
    private $token;

    public function __construct(string $email, int $token)
    {
        $this->email = $email;
        $this->token = $token;
    }

    public function handle()

    {
        Mail::to($this->email)->send(new RegisterMail([
            'subject' => 'register',
            'message' => 'confirm code',
            'email' => $this->email,
            'token' => $this->token,
        ]));


    }
}
