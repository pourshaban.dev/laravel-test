<?php


namespace App\Traits\Api\V1\Response;


trait ApiResponseTrait
{
    protected function success($data = [], $message = '', $status = 200)
    {
        return response([
            'success' => true,
            'data' => $data,
            'message' => $message,
        ], $status);
    }

    protected function failure($message, $status = 400)
    {
        return response([
            'success' => false,
            'data' => null,
            'message' => $message,
        ], $status);
    }
}
