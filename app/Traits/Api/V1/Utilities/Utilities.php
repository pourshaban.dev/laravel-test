<?php


namespace App\Traits\Api\V1\Utilities;


trait Utilities
{
    protected function convertFaToEnNumber($number)
    {
        $engNumber = str_replace(
            array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'),
            array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'),
            $number);
        return $engNumber;
    }

    protected function getOnlyEnNumber($number)
    {
        if (strpos($number, '+') !== false) {
            $engNumber = number_format($number);
            $engNumber = preg_replace("/[^0-9.]/", "", $engNumber);
        } else {
            $engNumber = str_replace(
                array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'),
                array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'),
                $number);
            $engNumber = preg_replace("/[^0-9.]/", "", $engNumber);
        }
        return $engNumber;
    }

    protected function getFloatNumber($number)
    {
        try {
            $number = str_replace(
                array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'),
                array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'),
                $number);
            $float = round($number * 100000000) / 100000000;
            return preg_replace("/[^0-9.]/", "", number_format(floatval($float), 8));
        } catch (\Exception $exception) {
            return 0;
        }
    }

    protected function getFloatNumberWithSpecificDecimalCount($number, $decimal = 8)
    {
        try {
            $number = str_replace(
                array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'),
                array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'),
                $number);
            $digit = explode('.', strval($number));
            $finalNumber = intval($digit[0]);
            if (isset($digit[1])) {
                $decimal = substr($digit[1], 0, $decimal);
                $finalNumber = floatval($digit[0] . '.' . $decimal);
            }
            return $finalNumber;
        } catch (\Exception $exception) {
            return 0;
        }
    }

    protected function getVertaTime(string $date, $isBegin = true)
    {
        $date = str_replace(
            array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'),
            array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'),
            $date);
        $arrayDate = explode('/', $date);
        $v = verta();
        if ($isBegin) {
            $v = $v->setDateTime($arrayDate[0], $arrayDate[1], $arrayDate[2], 0, 0, 0);
        } else {
            $v = $v->setDateTime($arrayDate[0], $arrayDate[1], $arrayDate[2], 23, 59, 59);
        }
        return $v;
    }

    protected function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

}
