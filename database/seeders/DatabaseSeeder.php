<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        $user = \App\Models\User::create([
            'name' => 'mamad',
            'email' => 'mamad@mamad.com',
            'password' => bcrypt('test'),
            'email_verified' => true,
            'email_token' => 123456
        ]);
        $role1 = \App\Models\Role::create([
            'is_active' => true,
            'title' => 'master access',
            'type' => 'master',
        ]);
        $role2 = \App\Models\Role::create([
            'is_active' => true,
            'title' => 'author access',
            'type' => 'author',
        ]);
        $user->roles()->sync([$role1->id, $role2->id]);

    }
}
