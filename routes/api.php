<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:api')->namespace('App\Http\Controllers\Api\V1')->prefix('v1')->group(function () {
    Route::prefix('auth')->namespace('Auth')->group(function () {
        Route::post('login', 'LoginAuthController@login')->withoutMiddleware('auth:api');
        Route::post('register', 'RegisterAuthController@register')->withoutMiddleware('auth:api');
        Route::post('confirm_code', 'RegisterAuthController@confirmCode')->withoutMiddleware('auth:api');
    });
    Route::middleware('isAdmin')->prefix('users')->namespace('Users')->group(function () {
        Route::get('index', 'IndexUsersController@index');
    });
});
